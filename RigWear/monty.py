#!/usr/bin/env python

#Monty the Python Rigley Aruduino Interface
#Written by Mike Meaney in Dallas Texas 2016

#To avoid a world of pain for Windows deployments do what this man says at
#https://www.youtube.com/watch?v=6u2D-P-zuno

#import the required dependencies
from firebase import firebase       # This handles the Firebase Transactions
import serial                       # The library to handle the serial communication from the arduino
from serial.tools import list_ports # 
from unicurses import *             # Our Humble Curses GUI
import time

#The firebase object to post data to
firebase = firebase.FirebaseApplication('https://rigley2.firebaseio.com', None)



#---- Global Functions ------

# making the POST to FireBase
def firebasePost():
  result = firebase.post('/rigdata', {
    "Data" : {
      "durration" : "1000",
      "timeIn" : "1451686518116",
      "timeOut" : "1451686519116"
    },
    "PID" : "Debug",
    "RigID" : "Room4",
    "__v" : 0,
    "_id" : "55b2b6d294244fef23df85c9"
  })

  #print result
  return result

# get the list of Serial Ports
def getPorts():   
  ports_avaiable = list(list_ports.comports())
  return ports_avaiable


#---- The Program it's self ----

#1. Start up and search for Serial Ports.
#2. Display the ports found and ask the RA to choose one.
#3. Once Chosen, connect to that port 
#4. Show what that port is doing.
#5. Give 'em an option to quit. If they want lol

def main():
  stdscr = initscr() # Let's get this Curses Party started!
  start_color() # Bro, do you even paint with all the colors of the wind?
  #noecho()
  curs_set(True)

  #Get the terminal size
  max_y, max_x = getmaxyx(stdscr)

  #enable the keypad
  keypad(stdscr, True)

  #Define our Color Pairs
  init_pair(1, COLOR_RED, COLOR_WHITE)
  init_pair(2, COLOR_GREEN, COLOR_BLUE)


  mvaddstr(max_y/2,30,"Welcome to Monty, Rigley's Python Rig Driver", color_pair(1) + A_BOLD)
  mvaddstr((max_y/2)+1,27,"Please press return to continue or escape to quit")

  #Track if the Escape key was hit
  running = True
  while( running ):
    key = getch() #Wait for user input. Either return to continue or esc to quit
    # Escape - clear the screen an break out of the elif statement. 
    if(key == 27):
      running = False
      clear()
      break
    elif(key == 10 or key == 13):
      move (0,0)
      clear()

      #Get the listing of the Serial Ports
      thePorts = getPorts()
      
      #Ask the User to choose a port
      addstr("Please choose your port then press return")

      portIterator = 1; 
      for port in thePorts:
        mvaddstr(portIterator,0,'['+str(portIterator)+']' + str(port));
        portIterator += 1

      mvaddstr(portIterator,0, "Type selection here please >")
      selectedPort = getch()

      p = int(chr(selectedPort))

      #Verify that the choice is valid
      if( p <= len(thePorts) ):
        clear()
        selectedPort = thePorts[p-1]
        mvaddstr(0,0, "You have selected the following Port: " + str(selectedPort))
        #Now that we have a port choosen let's name this rig
        mvaddstr(1,0, "Please provide a name for this rig")
        mvaddstr(2,0, ">>")
        rigName = getstr()
        clear();
        mvaddstr(0, ((max_x/2)-(len(rigName)+3+(len(str(selectedPort))))), rigName + " @ " + str(selectedPort),color_pair(2) + A_BOLD )
        #Fantastic! Now that we have all the logistics sorted out, let's get to reading some data!! :D
        #We can declare the Serial object to speak with the Arduino and do all that great old-school VooDoo
        #ser = serial.Serial()
        mvaddstr(3,0, selectedPort)


      else:
        mvaddstr(10, 5, "Sorry pal, that's not a valid option. \n Press return to try again",color_pair(2) + A_BOLD)

  endwin() # We are done here. Kill the window
  return 0

# Now go do it!
if (__name__ == "__main__"):
  freeze_support()
  main()

#firebasePost()
#getPorts()

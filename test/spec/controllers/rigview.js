'use strict';

describe('Controller: RigviewCtrl', function () {

  // load the controller's module
  beforeEach(module('rigleyV2App'));

  var RigviewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RigviewCtrl = $controller('RigviewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});

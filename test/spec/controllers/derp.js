'use strict';

describe('Controller: DerpCtrl', function () {

  // load the controller's module
  beforeEach(module('rigleyV2App'));

  var DerpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DerpCtrl = $controller('DerpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});

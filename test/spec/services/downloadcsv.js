'use strict';

describe('Service: downloadCSV', function () {

  // load the service's module
  beforeEach(module('rigleyV2App'));

  // instantiate service
  var downloadCSV;
  beforeEach(inject(function (_downloadCSV_) {
    downloadCSV = _downloadCSV_;
  }));

  it('should do something', function () {
    expect(!!downloadCSV).toBe(true);
  });

});

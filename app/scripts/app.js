'use strict';

/**
 * @ngdoc overview
 * @name rigleyV2App
 * @description
 * # rigleyV2App
 *
 * Main module of the application.
 */
angular.module('rigleyV2App', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'firebase.ref',
    'firebase.auth'
  ]);

//RA View 

'use strict';

/**
 * @ngdoc function
 * @name rigleyV2App.controller:RaviewCtrl
 * @description
 * # RaviewCtrl
 * Controller of the rigleyV2App
 */


angular.module('rigleyV2App')
  .controller('RaviewCtrl', function ($scope, $firebaseObject) {
  
  // ref is the big ambiguious concept for the entire firebase 
  var ref = new Firebase("https://rigley2.firebaseio.com/rigdata");

  // download the entriety of the DB into a local object
  $scope.rigdatas = $firebaseObject(ref);

  //Once the Firebase has loaded get the rigdata out of it
  $scope.rigdatas.$loaded()
  	.then(function() {
  		$scope.dbStatus = "Database Succesfully Loaded";
  		//$scope.rigdatas = $scope.data.rigdata;

      //The Three-way data binding that Firebase is great at!
  	})
  	.catch(function(err){
  		//Otherwise alert the user that the DB isn't 
  		$scope.dbStatus = "Error Loading Database into Page"
  	});

  	//Function for CSV Generation
  	$scope.getBlob = function(){
  		alert("DATA!")
  		 var data = new Blob(["This is very broken"], {type: 'text/plain'});
    	 window.location.href = "data:text/csv;base64," + data;

    	 //
    }
 
  }); //EOF RA View Controller



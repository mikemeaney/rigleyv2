'use strict';

/**
 * @ngdoc function
 * @name rigleyV2App.controller:DerpCtrl
 * @description
 * # DerpCtrl
 * Controller of the rigleyV2App
 */
angular.module('rigleyV2App')
  .controller('DerpCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name rigleyV2App.controller:RigviewCtrl
 * @description
 * # RigviewCtrl
 * Controller of the rigleyV2App
 */
angular.module('rigleyV2App')
  .controller('RigviewCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

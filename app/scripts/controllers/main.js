'use strict';

/**
 * @ngdoc function
 * @name rigleyV2App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rigleyV2App
 */
angular.module('rigleyV2App')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

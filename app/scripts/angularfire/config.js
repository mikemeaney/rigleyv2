angular.module('firebase.config', [])
  .constant('FBURL', 'https://rigley2.firebaseio.com')
  .constant('SIMPLE_LOGIN_PROVIDERS', ['password'])

  .constant('loginRedirectPath', '/login');
